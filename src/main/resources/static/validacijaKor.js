function validacijaKorisnik(){
    if(confirm("Da li ste sigurni?") == true) {
        var date_regex = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
        let imeKor = document.getElementById("ime").value
        let prezimeKor = document.getElementById("prezime").value
        let usernameKor = document.getElementById("username").value
        let passwordKor = document.getElementById("password").value
        let datRodjKor = document.getElementById("datumRodjenja").value
        let adresaKor = document.getElementById("adresa").value
        let brTelKor = document.getElementById("brTelefona").value
        let emailKor = document.getElementById("email").value

        if (imeKor == "") {
            alert('Morate uneti ime korisnika');
            return false;
        }
        else if (prezimeKor == "") {
            alert('Morate uneti prezime korisnika');
            return false;
        }
        else if (usernameKor == "") {
            alert('Morate uneti username korisnika');
            return false;
        } else if (passwordKor == "") {
            alert('Morate uneti password korisnika!');
            return false;
        }
        else if (adresaKor == "") {
            alert('Morate uneti adresu korisnika!');
            return false;
        }
        else if (brTelKor == "") {
            alert('Morate uneti broj telefona korisnika!');
            return false;
        }
        else if (emailKor == "") {
            alert('Morate uneti email korisnika!');
            return false;
        }
        else if (!(date_regex.test(datRodjKor))) {
            alert('Format datuma rodjenja nije dobar,unesite ga u formatu YYYY-mm-dd');
            return false;
        }
    }
    else {
        return false;
    }




}

