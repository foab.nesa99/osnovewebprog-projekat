



function validacijaKnjiga(){
    if(confirm("Da li ste sigurni?") == true) {

        let naziv = document.getElementById("naziv").value
        let izdavackaKuca = document.getElementById("izdavackaKuca").value
        let autor = document.getElementById("autor").value
        let opis = document.getElementById("opis").value
        let cena = document.getElementById("cena").value
        let godinaIzdavanja = document.getElementById('godinaIzdavanja').value
        let tipPoveza = document.getElementById("povez").value
        let brojStranica = document.getElementById("brojStranica").value
        let pismo = document.getElementById("pismoKnjige").value
        let jezik = document.getElementById("jezik").value
        let ocena = document.getElementById('prosecnaOcena').value
        let slika = document.getElementById("slika").value
        let zanr = document.getElementById("zanr").value

        if (jezik == "") {
            alert('Jezik knjige ne moze da bude prazan!');
            return false;
        }
        else if (zanr == "") {
            alert('Zanr knjige ne moze da bude prazan!');
            return false;
        }
        else if (naziv == "") {
            alert('Naziv knjige ne moze da bude prazan!');
            return false;
        } else if (izdavackaKuca == "") {
            alert('Izdavacka kuca ne moze da bude prazna!');
            return false;
        } else if (autor == "") {
            alert('Morate uneti autora knjige!');
            return false;
        } else if (opis == "") {
            alert('Opis knjige ne moze da bude prazan!');
            return false;
        } else if (cena == "") {
            alert('Cena knjige ne moze da bude prazna!');
            return false;
        } else if (godinaIzdavanja == "") {
            alert('Morate uneti godinu izdavanja knjige!');
            return false;
        } else if (brojStranica == "") {
            alert('Morate uneti broj stranica knjige!');
            return false;
        } else if (ocena == "") {
            alert('Morate uneti prosecnu ocenu knjige!');
            return false;
        } else if (slika == "") {
            alert('Molim,unesite url slike za koricu knjige.Ako slika ne postoji ,upisite !NEMA!')
            return false;
        }
        else if(isNaN(ocena) || ocena<0 || ocena >5){
            alert("Ocena mora biti broj izmedju 0 i 5");
            return false
        }
        else if(isNaN(brojStranica)){
            alert("Broj stranica mora biti broj!");
            return false
        }
        else if(isNaN(godinaIzdavanja)){
            alert("Godina izdavanja mora biti broj");
            return false
        }
        else if(isNaN(cena) || cena<0){
            alert("Cena mora biti broj veci od nule!");
            return false
        }
    }
    else {
        return false;
    }




}

