package com.ftn.OWPspring.repo;

import com.ftn.OWPspring.model.Korisnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface KorisnikRepository  {
    Optional<Korisnik> findByUsername(String username);



}
