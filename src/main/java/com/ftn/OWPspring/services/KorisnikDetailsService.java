package com.ftn.OWPspring.services;

import com.ftn.OWPspring.model.Korisnik;
import com.ftn.OWPspring.model.KorisnikDetails;
import com.ftn.OWPspring.repo.KorisnikRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class KorisnikDetailsService implements UserDetailsService {
    private final KorisnikRepository korisnikRepository;

    public KorisnikDetailsService(KorisnikRepository korisnikRepository) {
        this.korisnikRepository = korisnikRepository;
    }

    @Override
    public KorisnikDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Korisnik> korisnik = korisnikRepository.findByUsername(username);
        korisnik.orElseThrow(() -> new UsernameNotFoundException("Nije pronadjen korisnik sa username :" + username));
        return korisnik.map(KorisnikDetails::new).get();
    }

}
