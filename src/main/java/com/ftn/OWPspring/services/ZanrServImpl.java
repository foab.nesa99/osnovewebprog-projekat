package com.ftn.OWPspring.services;

import com.ftn.OWPspring.model.Zanr;
import com.ftn.OWPspring.repo.ZanrRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ZanrServImpl implements ZanrService{

    @Autowired
    ZanrRepository zanrRepository;


    @Override
    public Optional<Zanr> vratiPoId(Long id) {
        return zanrRepository.findById(id);
    }

    @Override
    public List<Zanr> vratiSveZanrove() {
        return zanrRepository.findAll();
    }
}
