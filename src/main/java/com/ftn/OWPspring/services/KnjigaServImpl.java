package com.ftn.OWPspring.services;

import com.ftn.OWPspring.model.Knjiga;
import com.ftn.OWPspring.model.Korisnik;
import com.ftn.OWPspring.repo.KnjigaRepository;
import com.ftn.OWPspring.repo.KorisnikRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class KnjigaServImpl implements KnjigaService{

    @Autowired
    KnjigaRepository knjigaRepository;

    @Override
    public List<Knjiga> vratiSveKnjige() {
        return knjigaRepository.findAll();
    }

    @Override
    public void sacuvajKnjigu(Knjiga knjiga) {
        this.knjigaRepository.save(knjiga);

    }

    @Override
    public Knjiga vratiKnjigaISBN(Long isbn) {
        Optional<Knjiga> optional = knjigaRepository.findById(isbn);
        Knjiga knjiga = null;
        if(optional.isPresent()){
            knjiga = optional.get();
        }
        else {
            throw new RuntimeException("Nije pronadjena ni jedna knjiga sa ID-om :" +isbn);

        }
        return knjiga;
    }
}