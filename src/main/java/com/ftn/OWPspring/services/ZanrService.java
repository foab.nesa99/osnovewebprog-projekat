package com.ftn.OWPspring.services;

import com.ftn.OWPspring.model.Zanr;

import java.util.List;
import java.util.Optional;


public interface ZanrService {
    Optional<Zanr> vratiPoId(Long id);
    List<Zanr> vratiSveZanrove();

}
