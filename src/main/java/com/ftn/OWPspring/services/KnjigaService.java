package com.ftn.OWPspring.services;

import com.ftn.OWPspring.model.Knjiga;
import org.springframework.stereotype.Service;

import java.util.List;


public interface KnjigaService {
    List<Knjiga> vratiSveKnjige();
    void sacuvajKnjigu(Knjiga knjiga);
    Knjiga vratiKnjigaISBN(Long isbn);
}
