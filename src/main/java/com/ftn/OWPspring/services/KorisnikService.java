package com.ftn.OWPspring.services;

import com.ftn.OWPspring.model.Korisnik;
import org.springframework.stereotype.Service;

import java.util.List;

public interface KorisnikService {

    List<Korisnik> vratiSveKorisnike();
    void sacuvajKorisnik(Korisnik korisnik);
    Korisnik vratiKorisnikId(Long id);
    Korisnik vratiPoUserName(String username);
}
