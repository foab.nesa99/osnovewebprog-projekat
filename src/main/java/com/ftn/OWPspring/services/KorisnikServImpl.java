package com.ftn.OWPspring.services;


import com.ftn.OWPspring.model.Korisnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ftn.OWPspring.repo.KorisnikRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class KorisnikServImpl implements KorisnikService{

    @Autowired
    KorisnikRepository korisnikRepository;


    @Override
    public List<Korisnik> vratiSveKorisnike() {
        return korisnikRepository.findAll();
    }

    @Override
    public void sacuvajKorisnik(Korisnik korisnik){
        this.korisnikRepository.save(korisnik);
        korisnik.setRole("ROLE_USER");
        korisnik.setDatReg(LocalDateTime.now());
    }

    @Override
    public Korisnik vratiKorisnikId(Long id) {
        Optional<Korisnik> optional = korisnikRepository.findById(id);
        Korisnik korisnik = null;
        if(optional.isPresent()){
            korisnik = optional.get();
        }
        else {
            throw new RuntimeException("Nije pronadjen korisnik sa ID-om :" +id);

        }
        return korisnik;
    }

    @Override
    public Korisnik vratiPoUserName(String username) {
        return korisnikRepository.findByUsername(username).orElseThrow(NoSuchElementException::new);
    }


}
