package com.ftn.OWPspring.model;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class KupljenaKnjiga {
 private Knjiga knjiga;
 private Integer brojPrimeraka;
 private Integer cena;



}
