package com.ftn.OWPspring.model;

import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



@Data
@NoArgsConstructor
public class Knjiga {


    public List<Zanr> zanrList;
    private String naziv;
    private Long isbn;
    private String izdavackaKuca;
    private String autor;
    private Integer godinaIzdavanja;
    private String opis;
    private String slika;
    private Integer cena;
    private Integer brojStranica;
    public enum tipPoveza{
        Tvrdi_povez,
        Meki_povez
    }

    public enum Pismo{
        Cirilica,
        Latinica
    }
    private tipPoveza povez;
    private Pismo pismoKnjige;
    private String jezik;
    private float prosecnaOcena;
    private String zanrovi;
}
