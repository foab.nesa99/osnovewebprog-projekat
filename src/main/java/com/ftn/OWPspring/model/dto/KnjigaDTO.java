package com.ftn.OWPspring.model.dto;



import com.ftn.OWPspring.model.Knjiga;
import com.ftn.OWPspring.model.Zanr;
import lombok.Data;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


@Data
public class KnjigaDTO {

    public List<Zanr> zanrList;

    @NotBlank(message = "Naziv ne moze biti prazan.")
    private String naziv;
    @NotBlank(message = "ISBN ne moze biti prazan.")
    @Size(min = 13, max = 13, message = "ISBN mora imati 13 karaktera.")
    private Long isbn;
    @NotBlank(message = "Izdavacka kuca ne moze biti prazna.")
    private String izdavackaKuca;
    @NotBlank(message = "Autor ne moze biti prazan.")
    private String autor;
    @NotBlank(message = "Godina izdavanja ne moze biti prazna.")
    private Integer godinaIzdavanja;
    @NotBlank(message = "Opis ne moze biti prazan.")
    private String opis;
    @NotBlank(message = "Slika ne moze biti prazna.")
    private String slika;
    @NotBlank(message = "Cena ne moze biti prazna.")
    private Integer cena;
    @NotBlank(message = "Broj stranica ne moze biti prazan.")
    private Integer brojStranica;

    private Knjiga.tipPoveza povez;
    private Knjiga.Pismo pismoKnjige;
    private String jezik;
    private float prosecnaOcena;
    private String zanrovi;
}
}
