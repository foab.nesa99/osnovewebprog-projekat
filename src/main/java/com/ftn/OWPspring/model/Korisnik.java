package com.ftn.OWPspring.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor

public class Korisnik {

    private Long id;
    @NotBlank(message = "Korisnicko ime ne sme biti prazno")
    private String username;
    @NotBlank(message = "Sifra ne sme biti prazna")
    private String password;

    private String role;
    @Email
    @NotBlank(message = "Email ne sme biti prazan")
    private String email;
    @NotBlank(message = "Unesite datum rodjenja")
    @DateTimeFormat(pattern ="yyyy-MM-dd")
    private Date datumRodjenja;
    @NotBlank(message = "Adresa ne sme biti prazna")
    private String adresa;
    @NotBlank(message = "Ime ne sme biti prazno")
    private String ime;
    @NotBlank(message = "Prezime ne sme biti prazno")
    private String prezime;
    @NotBlank(message = "Morate uneti br telefona")
    private String brTelefona;
    private LocalDateTime datReg ;
    private boolean active = true;




}
