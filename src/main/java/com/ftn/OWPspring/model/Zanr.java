package com.ftn.OWPspring.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Entity
@Data
@NoArgsConstructor
@Table(name="zanr")
public class Zanr {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "knjiga")
    @ManyToMany(mappedBy = "zanrList")
    private List<Knjiga> knjigaList;
    @Column(nullable = false)
    private String ime;
    @Column(nullable = false)
    private String opis;


}
