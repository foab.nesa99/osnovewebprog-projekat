package com.ftn.OWPspring.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class Komentar {
    private String tekst;
    private int ocena;
    private Date datum;
    private Korisnik autor;
    private Knjiga knjiga;
    public enum Status{
        Na_cekanju,
        Odobren,
        Nije_odobren

    }
    private Status status;
}
