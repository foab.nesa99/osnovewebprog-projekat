package com.ftn.OWPspring.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class Kupovina {


private List<KupljenaKnjiga> listaKupljenih = new ArrayList<>();
    private Integer ukupnaCena;
    private Date datum;
    private Korisnik korisnik;
    private Integer brojKupljenih;


}
