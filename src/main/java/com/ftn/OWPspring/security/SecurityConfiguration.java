package com.ftn.OWPspring.security;


import com.ftn.OWPspring.services.KorisnikDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    private final KorisnikDetailsService korisnikDetailsService;

    public SecurityConfiguration(KorisnikDetailsService korisnikDetailsService) {
        this.korisnikDetailsService = korisnikDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(korisnikDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/korisnici").hasRole("ADMIN")
                .antMatchers("/saveUser").permitAll()
                .antMatchers("/jednaKnjiga/**").permitAll()
                .antMatchers("/showFormForUpdate/**").hasRole("ADMIN")
                .antMatchers("/updateKnjiga*").hasRole("ADMIN")
                .antMatchers("/dodajKnjigu").hasRole("ADMIN")
                .antMatchers("/dodajKnjiga").hasRole("ADMIN")
                .antMatchers("/viewUserProfile").hasRole("USER")
                .antMatchers("/login*").permitAll()
                .antMatchers("/static/**").permitAll()
                .antMatchers("/").permitAll()
                .and().formLogin().loginPage("/login")
                .defaultSuccessUrl("/",true)
                .and().logout().deleteCookies("JSSESIONID").permitAll();
        http.csrf().disable();
        http.sessionManagement().maximumSessions(3).maxSessionsPreventsLogin(true);
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }


}
