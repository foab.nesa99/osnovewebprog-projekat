package com.ftn.OWPspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class OwPspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(OwPspringApplication.class, args);
	}

}
