package com.ftn.OWPspring.controller;

import com.ftn.OWPspring.model.Knjiga;

import com.ftn.OWPspring.model.Zanr;
import com.ftn.OWPspring.repo.KnjigaRepository;
import com.ftn.OWPspring.repo.ZanrRepository;
import com.ftn.OWPspring.services.KnjigaService;

import com.ftn.OWPspring.services.KorisnikService;
import com.ftn.OWPspring.services.ZanrService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@Controller
public class KnjigaController {
    @Autowired
   KnjigaService knjigaService;



    @Autowired
    ZanrRepository zanrRepository;


    private final KorisnikService korisnikService;
    private final KnjigaRepository knjigaRepository;

    private final ZanrService zanrService;

    public KnjigaController(KnjigaRepository knjigaRepository,KorisnikService korisnikService, KnjigaService knjigaService, ZanrService zanrService) {
        this.knjigaRepository = knjigaRepository;
        this.korisnikService = korisnikService;
        this.knjigaService = knjigaService;
        this.zanrService = zanrService;
    }


    @GetMapping("/jednaKnjiga/{isbn}")
    public String jednaKnjigaDet(@PathVariable(value = "isbn") Long isbn, Model model) {
        Knjiga knjiga = knjigaService.vratiKnjigaISBN(isbn);
        val listZanr = zanrService.vratiSveZanrove();
        val listZanrKnjigaID = knjiga.getZanrList().stream().map(Zanr::getId).collect(Collectors.toList());
        model.addAttribute("knjiga", knjiga);
        model.addAttribute("listZanr", listZanr);

        return "jednaKnjiga";
    }

    @GetMapping("/")
    public String homePage(Model model){
        model.addAttribute("listaKnjiga",knjigaService.vratiSveKnjige());
        model.addAttribute("listZanr" , zanrRepository.findAll());
        return "knjige";

    }

    @GetMapping("/knjige12")
        public String knige(Model model){
            model.addAttribute("listaKnjiga",knjigaService.vratiSveKnjige());
            model.addAttribute("listZanr" , zanrRepository.findAll());
            return "knjige12";
    }



    @GetMapping("/dodajKnjiga")
    public String dodajKnjiga(Model model){
        Knjiga knjiga = new Knjiga();
        model.addAttribute("knjiga" ,knjiga);

        return "nova_knjiga";
    }
    @PostMapping("/dodajKnjigu")
    public String dodajKnjigu(@ModelAttribute("knjiga") Knjiga knjiga){


        knjigaService.sacuvajKnjigu(knjiga);
        return "redirect:/knjige12";
    }
    @GetMapping("/updateKnjiga/{isbn}")
    public String updateknjiga(@PathVariable(value = "isbn") long isbn, Model model){
        Knjiga knjiga = knjigaService.vratiKnjigaISBN(isbn);
        model.addAttribute("knjiga" ,knjiga);
        return "update_knjiga";


    }


}
