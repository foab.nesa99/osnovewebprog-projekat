package com.ftn.OWPspring.controller;


import com.ftn.OWPspring.model.Korisnik;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.ftn.OWPspring.services.KorisnikService;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDateTime;

@Controller
public class KorisnikController {


    @Autowired
    KorisnikService korisnikService;


    @GetMapping("/korisnici")
    public String homePage(Model model){
    model.addAttribute("listaKorisnika",korisnikService.vratiSveKorisnike());
    return "korisnici";

    }

    @GetMapping("/korisnici/dodajKorisnik")
    public String dodajKorisnik(Model model){
        Korisnik korisnik = new Korisnik();
        model.addAttribute("korisnik" ,korisnik);

        return "novi_korisnik";
    }
    @PostMapping("/korisnici/dodajKorisnika")
    public String dodajKorisnika(@ModelAttribute("korisnik") Korisnik korisnik){
        korisnik.setDatReg(LocalDateTime.now());
        korisnik.setRole("ROLE_USER");
        korisnikService.sacuvajKorisnik(korisnik);
        return "redirect:/korisnici";
    }
    @GetMapping("/korisnici/updateKorisnik/{id}")
    public String updateKorisnik(@PathVariable (value = "id") long id,Model model){
        Korisnik korisnik = korisnikService.vratiKorisnikId(id);
        model.addAttribute("korisnik" ,korisnik);
        return "update_korisnik";


    }

}
