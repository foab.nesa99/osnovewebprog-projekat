package com.ftn.OWPspring.controller;

import com.ftn.OWPspring.services.KnjigaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class KorpaController {

    @Autowired
    KnjigaService knjigaService;

    @GetMapping("/korpa")
    public String korisnickaKorpa(Authentication authentication, Model model){

        model.addAttribute("listaKnjiga",knjigaService.vratiSveKnjige());

        return "korpaPg";
    }


}
